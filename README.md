# squid-alpine without root

`squid` without `root` privilege

# usage

this example uses `3228/tcp` port

`docker run --rm -p 3228:3128 registry.gitlab.com/k9i-public/squid-alpine:main`

OR

`docker run --rm -p 3128:3128 -v <path_to_squid.conf>:/etc/squid/squid.conf registry.gitlab.com/k9i-public/squid-alpine:main`
