#!/bin/sh
set -eu

entrypoint_log() {
  if [ "${ENTRYPOINT_QUIET_LOGS:-}" = "YES" ]; then
    echo "$@"
  fi
}

run_entrypoint_d() {
  local f
  if [ ! -d /entrypoint.d ]; then
    entrypoint_log "$0: /entrypoint.d/ is empty; skipped"
    return 0
  fi
  for f in /entrypoint.d/*; do
    case "$f" in
      *.envsh)
        if [ -x "$f" ]; then
          entrypoint_log "$0: Sourcing $f";
          . "$f"
        else
          entrypoint_log "$0: Ignoring $f, not executable";
        fi
        ;;
      *.sh)
        if [ -x "$f" ]; then
          entrypoint_log "$0: Launching $f";
          "$f"
        else
          entrypoint_log "$0: Ignoring $f, not executable";
        fi
        ;;
      *) entrypoint_log "$0: Ignoring $f";;
    esac
  done

  entrypoint_log "$0: Configuration complete; ready for start up"
}

# ----------------------------------------------------------------------
# Initialze default variables
squid_conf=${SQUID_CONF:-/etc/squid/squid.conf}
create_swap=${SQUID_CREATE_SWAP:-YES}

# ----------------------------------------------------------------------
# create swap
#
if [ "$create_swap" = "YES" ]; then
  squid -z --foreground
fi

# ----------------------------------------------------------------------
# replace suqid log file to pipe (for tail to stdout)
if [ "${SQUID_USE_CAT_LOG:-}" = "YES" ]; then
  # NOTICE: not tested with /var/cache/squid/netdb.state
  for logfile in /var/log/squid/*.log /var/cache/squid/netdb.state; do
    if [ -f "$logfile" ]; then
      rm -v $logfile
      ( set -x; mknod -m 777 $logfile p )
      cat ${logfile} &  # cat squid piped logfile to stdout
    fi
  done
fi

# ----------------------------------------------------------------------
# parse arg, then run
#
default_opts="-f ${squid_conf} -NYC"

if [ $# -ne 0 ]; then
  case "${1:-}" in
    -*)  # run squid with user specified args
      default_opts=''
      ;;
    squid|*/squid)
      shift  # remove `squid` (command name) from args
      ;;
    sh)  # invoke sh
      exec "$@"
      ;;
    *)  # other than squid, exec as command
      exec "$@"
      ;;
  esac
fi

# ----------------------------------------------------------------------
run_entrypoint_d
squid --version

# ----------------------------------------------------------------------
# show env
_envs="SQUID_CONF SQUID_CREATE_SWAP SQUID_USE_CAT_LOG"
for _env in ${_envs}; do
  eval "echo \"# ==> INFO: $_env=\${${_env}:-}\""
done

# ----------------------------------------------------------------------
# run squid with this container default args and user specified args
set -x
exec squid ${default_opts:-} "$@"
