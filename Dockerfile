FROM alpine:3.20.3

RUN set -eu \
    && apk add --no-cache squid \
    && true

RUN set -eu \
    && touch /var/log/squid/access.log /var/log/squid/store.log /var/log/squid/cache.log /var/cache/squid/netdb.state \
    && chown -R squid:squid /var/log/squid /run/squid /var/cache/squid \
    && chmod a+rw /run /var/log/squid /var/cache/squid \
    && (cd /usr/share/squid/errors && ln -s en ja ) \
	&& echo "shutdown_lifetime 3 seconds # use smaller timeout than docker default(10s)" >> /etc/squid/squid.conf \
	&& echo "max_filedescriptors 16384 # avoid xcalloc error" >> /etc/squid/squid.conf \
    && true

COPY files /

EXPOSE 3128/tcp

USER squid:squid

#VOLUME [ "/etc/squid", "/var/cache/squid" ]
VOLUME [ "/var/cache/squid" ]

# for squid coredump_dir
WORKDIR /tmp

ENTRYPOINT [ "/entrypoint.sh" ]

RUN set -eux; \
	#cat /etc/os-release; \
	squid -v
